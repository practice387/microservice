package com.ertelecom.microservice.repository;

import com.ertelecom.microservice.domain.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcBookRepository implements BookRepository{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(Book book) {
        return jdbcTemplate.update("INSERT INTO books (title,description) VALUES(?,?)",
                new Object[]{book.getTitle(),book.getDescription()});
    }

    @Override
    public int update(Book book) {
        return jdbcTemplate.update("UPDATE books SET title=?,description=? WHERE id=?",
                new Object[]{book.getTitle(),book.getDescription(),book.getId()});
    }

    @Override
    public Book findById(int id) {
        try{
            Book book=jdbcTemplate.queryForObject("SELECT * FROM books WHERE id=?",
                    BeanPropertyRowMapper.newInstance(Book.class), id);
            return book;
        }
        catch (IncorrectResultSizeDataAccessException e){
            return null;
        }
    }

    @Override
    public int deleteById(int id) {
        return jdbcTemplate.update("DELETE FROM books WHERE id=?", id);
    }

    @Override
    public List<Book> findAll() {
        return jdbcTemplate.query("SELECT * from books", BeanPropertyRowMapper.newInstance(Book.class));
    }
}
