package com.ertelecom.microservice.repository;

import com.ertelecom.microservice.domain.Book;

import java.util.List;

public interface BookRepository {
    int save(Book book);
    int update(Book book);
    Book findById(int id);
    int deleteById(int id);
    List<Book> findAll();
}
