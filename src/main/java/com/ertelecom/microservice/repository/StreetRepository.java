package com.ertelecom.microservice.repository;

import com.ertelecom.microservice.domain.Street;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StreetRepository extends JpaRepository<Street,Long> {
    Street findByNamestreet(String name);
    Street findById(long id);
}