package com.ertelecom.microservice.repository;

import com.ertelecom.microservice.domain.Cities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<Cities,Long> {
    Cities findByNamecity(String name);
    Cities findById(long id);
}
