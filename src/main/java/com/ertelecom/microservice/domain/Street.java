package com.ertelecom.microservice.domain;

import lombok.Data;

import javax.persistence.*;


@Entity
@Data
public class Street{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "namestreet", length = 32)
    private String namestreet;

    @ManyToOne
    @JoinColumn(name = "id_cities")
    private Cities city;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cities getCity() {
        return this.city;
    }

    public void setCity(Cities city) {
        this.city = city;
    }

    public Street(String namestreet) {
        this.namestreet = namestreet;
    }

    public Street() {
    }


    public String getNamestreet() {
        return namestreet;
    }
    public void setNamestreet(String namestreet) {
        this.namestreet = namestreet;
    }

    @Override
    public String toString() {
        return "Street{" +
                "namestreet='" + namestreet + '\'' +
                '}';
    }
}
