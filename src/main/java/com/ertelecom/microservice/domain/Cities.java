package com.ertelecom.microservice.domain;



import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Accessors(chain = true)
public class Cities {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String namecity;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "city")
    private Set<Street> street=new HashSet<>();

    public Cities(String name) {
        this.namecity=name;
    }

    public Cities() {

    }
}
