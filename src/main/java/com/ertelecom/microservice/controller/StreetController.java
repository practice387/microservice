package com.ertelecom.microservice.controller;

import com.ertelecom.microservice.domain.Street;
import com.ertelecom.microservice.repository.CityRepository;
import com.ertelecom.microservice.repository.StreetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/Street")
public class StreetController {

    private final CityRepository cityRepository;
    private final StreetRepository streetRepository;
    private Logger logger = LoggerFactory.getLogger(CitiesController.class);

    public StreetController(CityRepository cityRepository, StreetRepository streetRepository) {
        this.cityRepository = cityRepository;
        this.streetRepository = streetRepository;
    }

    @GetMapping("/findById{id}")
    public Street findStreetById(@PathVariable long id){
        logger.info("Collecting street by {id}", id);
        return streetRepository.findById(id);
    }

    @GetMapping("/findByName{name}")
    public Street findStreetByName(@PathVariable String name){
        logger.info("Collecting street by {name}", name);
        return streetRepository.findByNamestreet(name);
    }

    @GetMapping("/findAll")
    public List<Street> findAll(){
        logger.info("Collecting all streets");
        return streetRepository.findAll();
    }

    @PostMapping("/saveStreet{CityId}")
    public Street saveStreet(@PathVariable(value = "CityId") int CityId,
@RequestBody Street streetRequest){
        logger.info("Add new street {street}", streetRequest);
        streetRequest.setCity(cityRepository.findById(CityId));
        return streetRepository.save(streetRequest);
    }

    @PutMapping("/updateStreet")
    public Street updateStreet(@RequestBody Street street){
        logger.info("Update street {street}", street);
        return streetRepository.save(street);
    }

    @DeleteMapping("/deleteStreetById{id}")
    public void deleteStreet(@PathVariable Integer id){
        logger.info("Delete street by {id}", id);
        streetRepository.delete(streetRepository.findById(id));
    }
}
