package com.ertelecom.microservice.controller;

import com.ertelecom.microservice.domain.Cities;
import com.ertelecom.microservice.repository.CityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/Cities")
public class CitiesController {

    private final CityRepository repository;
    private Logger logger = LoggerFactory.getLogger(CitiesController.class);

    public CitiesController(CityRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/findById{id}")
    public Cities findCityById(@PathVariable long id){
        logger.info("Collecting city by {id}", id);
        return repository.findById(id);
    }
    @GetMapping("/findByName{name}")
    public Cities findCityByName(@PathVariable String name){
        logger.info("Collecting city by {name}", name);
        return repository.findByNamecity(name);
    }
    @GetMapping("/findAll")
    public Iterable<Cities> findAll(){
        logger.info("Collecting all cities");
        return repository.findAll();
    }
    @PostMapping("/saveCity")
    public Cities saveCity(@RequestBody Cities city){
        logger.info("Add new city {city}", city);
        return repository.save(city);
    }
    @PutMapping("/updateCity")
    public Cities updateCity(@RequestBody Cities city){
        logger.info("Update city {city}", city);
        return repository.save(city);
    }
    @DeleteMapping("/deleteCityById{id}")
    public void deleteCity(@PathVariable int id){
        logger.info("Delete city by {id}", id);
        repository.delete(repository.findById(id));
    }
}
