package com.ertelecom.microservice.controller;

import com.ertelecom.microservice.domain.Book;
import com.ertelecom.microservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/JdbcTest")
public class BookController {
    @Autowired
    BookRepository bookRepository;

    @PostMapping("/saveBook")
    public void saveBook(@RequestBody Book book) {
        bookRepository.save(new Book(book.getTitle(),book.getDescription()));
    }
    @PutMapping("/updateBook{id}")
    public void updateBook(@PathVariable("id") int id,@RequestBody Book book){
        Book _book=bookRepository.findById(id);
        if(_book!=null){
            _book.setId(id);
            _book.setTitle(book.getTitle());
            _book.setDescription(book.getDescription());
            bookRepository.update(_book);
        }
        bookRepository.update(book);
    }
    @DeleteMapping("/deleteBook{id}")
    public void deleteBook(@PathVariable int id){
        bookRepository.deleteById(id);
    }
    @GetMapping("/findAll")
    public List<Book> findAll(){
        return bookRepository.findAll();
    }
    @GetMapping("/findById{id}")
    public Book findBookById(@PathVariable Integer id){
        return bookRepository.findById(id);
    }
}
