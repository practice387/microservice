package com.ertelecom.microservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestLogSLF4j {
    public void StartSlf4(){
        Logger logger = LoggerFactory.getLogger(TestLogSLF4j.class);
        logger.info("anything text");
    }
}
