CREATE TABLE cities(
    id_cities SERIAL PRIMARY KEY NOT NULL,
    namecity VARCHAR(255)
);
CREATE TABLE street
(
    id SERIAL PRIMARY KEY NOT NULL ,
    namestreet VARCHAR(255),
    id_cities INTEGER,
    FOREIGN KEY (id_cities) REFERENCES cities (id_cities)
);
CREATE TABLE books
(
    id SERIAL PRIMARY KEY NOT NULL ,
    title VARCHAR(255),
    description VARCHAR(255)
);