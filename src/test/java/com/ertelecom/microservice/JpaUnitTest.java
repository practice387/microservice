package com.ertelecom.microservice;

import com.ertelecom.microservice.domain.Cities;
import com.ertelecom.microservice.domain.Street;
import com.ertelecom.microservice.repository.CityRepository;
import com.ertelecom.microservice.repository.StreetRepository;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@AutoConfigureTestDatabase
@DataJpaTest
public class JpaUnitTest {

    @Autowired
    private CityRepository cityRepository;
    @Autowired
    private StreetRepository streetRepository;
    @Test
    public void EmptyCities(){
       Iterable city=cityRepository.findAll();
       assertThat(city).isEmpty();
    }
    @Test
    public void EmptyStreet(){
        Iterable street=streetRepository.findAll();
        assertThat(street).isEmpty();
    }
    @Test
    public void StoreInCities(){
        Cities city=cityRepository.save(new Cities("TestCity"));
        assertThat(city).hasFieldOrPropertyWithValue("namecity","TestCity");
    }
    @Test
    public void StoreStreet(){
        Street street=streetRepository.save(new Street("TestStreet"));
        assertThat(street).hasFieldOrPropertyWithValue("namestreet","TestStreet");
    }
    @Test
    public void FindAllCity(){
        Cities city1=new Cities("FirstTestCity");
        Cities city2=new Cities("SecondTestCity");
        Cities city3=new Cities("ThirdTestCity");
        cityRepository.save(city1);
        cityRepository.save(city2);
        cityRepository.save(city3);
        Iterable<Cities> city=cityRepository.findAll();
        assertThat(city).contains(city1,city3,city2);
    }
    @Test
    public void FindAllStreet(){
        Street street1=new Street("FirstTestStreet");
        Street street2=new Street("SecondTestStreet");
        Street street3=new Street("ThirdTestStreet");
        streetRepository.save(street1);
        streetRepository.save(street2);
        streetRepository.save(street3);
        Iterable<Street> street=streetRepository.findAll();
        assertThat(street).hasSize(3).contains(street1,street2,street3);
    }
    @Test
    public void FindByIdCity(){
        Cities city1=new Cities("FirstTestCity");
        Cities city2=new Cities("SecondTestCity");
        cityRepository.save(city1);
        cityRepository.save(city2);
        Cities cities=cityRepository.findById(city1.getId()).get();
        assertThat(cities).isEqualTo(city1);
    }
    @Test
    public void FindByIdStreet(){
        Street street1=new Street("FirstTestStreet");
        Street street2=new Street("SecondTestStreet");
        streetRepository.save(street1);
        streetRepository.save(street2);
        Street street=streetRepository.findById(street1.getId()).get();
        assertThat(street).isEqualTo(street1);
    }
    @Test
    public void FindByNameCity(){
        Cities city1=new Cities("FirstTestCity");
        Cities city2=new Cities("SecondTestCity");
        cityRepository.save(city1);
        cityRepository.save(city2);
        Cities cities=cityRepository.findByNamecity("FirstTestCity");
        assertThat(cities).isEqualTo(city1);
    }
    @Test
    public void FindByNameStreet(){
        Street street1=new Street("FirstTestStreet");
        Street street2=new Street("SecondTestStreet");
        streetRepository.save(street1);
        streetRepository.save(street2);
        Street street=streetRepository.findByNamestreet("FirstTestStreet");
        assertThat(street).isEqualTo(street1);
    }
    @Test
    public void UpdateCity(){
        Cities city1=new Cities("FirstTestCity");
        cityRepository.save(city1);
        city1.setNamecity("NewTestCityName");
        cityRepository.save(city1);
        assertThat(city1.getNamecity()).isEqualTo("NewTestCityName");
    }
    @Test
    public void UpdateStreet(){
        Street street1=new Street("FirstTestStreet");
        streetRepository.save(street1);
        street1.setNamestreet("NewTestStreetName");
        streetRepository.save(street1);
        assertThat(street1.getNamestreet()).isEqualTo("NewTestStreetName");
    }
    @Test
    public void DeleteCityById(){
        Cities city1=new Cities("FirstTestCity");
        Cities city2=new Cities("SecondTestCity");
        Cities city3=new Cities("ThirdTestCity");
        cityRepository.save(city1);
        cityRepository.save(city2);
        cityRepository.save(city3);
        cityRepository.deleteById(city2.getId());
        Iterable<Cities> city= cityRepository.findAll();
        assertThat(city).hasSize(2).contains(city1,city3);
    }
    @Test
    public void DeleteStreetById(){
        Street street1=new Street("FirstTestStreet");
        Street street2=new Street("SecondTestStreet");
        Street street3=new Street("ThirdTestStreet");
        streetRepository.save(street1);
        streetRepository.save(street2);
        streetRepository.save(street3);
        streetRepository.deleteById(street2.getId());
        Iterable<Street> street=streetRepository.findAll();
        assertThat(street).hasSize(2).contains(street1,street3);
    }
}
